
function getCookieData(n) {
    let a = `; ${document.cookie}`.match(`;\\s*${n}=([^;]+)`);
    return a ? a[1] : '';
}

export function getCookie(name,cookies){
	let user_cake = ''
	let cake = getCookieData(name)
	if(cake !== ''){
		//console.log('Cookie from document')			
		user_cake = JSON.parse(decodeURIComponent(cake))	
	}else{
		//console.log('Cookie from server')
		user_cake = cookies.get(name)
	}
	return user_cake
}