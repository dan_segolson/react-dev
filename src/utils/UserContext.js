import React, { Component } from 'react'
import { Cookies } from 'react-cookie'
import { getCookie } from '../utils/functions'

const { Provider, Consumer } = React.createContext()

class UserProvider extends Component {
	

	state = {
		username : '', 
		authenticated: false,
		player_id: '',
		user_token: '',
		cookies: new Cookies()
	}


	componentDidMount(){
		
		let login_data = getCookie('user_context',this.state.cookies)
		if(!login_data){
			//console.log('No cookies')
		}else if(login_data.user_token !== ''){
			this.setState({authenticated: true, player_id: login_data.player_id, user_token: login_data.user_token, username: login_data.username})
		}
	}

	handleLogin = data => {
		if((data.user_token !== '') && (data.player_id !== '')){
			this.setState({authenticated: true, player_id: data.player_id, user_token: data.user_token, username: data.username})
			this.setCookie(data)
		}
		
	}

	handleLogout = data => {
		this.setState({authenticated: false, player_id: null, user_token: null, username: null})
		this.state.cookies.remove('user_context', {path: '/'})
	}

	setCookie = data => {
		const loginData = {player_id: data.player_id, user_token: data.user_token, username: data.username}
		let cookieData = JSON.stringify(loginData)
		var expiryDate = new Date(Date.now() + 60 * 60 * 1000) // 1 hour
		this.state.cookies.set('user_context',cookieData, {httpOnly: false, path: '/',expires:expiryDate} )
	}

	render() {
		return(
			<Provider value={{
				user: this.state.username,
				onLogin: this.handleLogin,
				onLogout: this.handleLogout,
				authenticated: this.state.authenticated,
				player_id: this.state.player_id, 
				user_token: this.state.user_token,
				}}>
				
			{this.props.children}
			</Provider>

			/*<Provider value={this.state}>
				{this.props.children}
			</Provider>*/
		)
	}
}

export { UserProvider, Consumer as UserConsumer}