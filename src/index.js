import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
import * as serviceWorker from './serviceWorker'
import ApolloClient from "apollo-boost";
import {ApolloProvider} from 'react-apollo'
import {InMemoryCache} from 'apollo-cache-inmemory'

import {GQL_URL} from './utils/constants'
import { UserProvider, UserConsumer }  from './utils/UserContext'

import App from './components/App'
import Login from './components/Login'
import { CookiesProvider } from 'react-cookie'
import './assets/index.css';
 
const client = new ApolloClient({
	uri: GQL_URL, 
	cache: new InMemoryCache()
})


function Root() {

	return(
		<UserProvider>
			<UserConsumer>
				{({user, authenticated, player_id, user_token, onLogin, onLogout}) => 
					user ? (<App />) : (<Login onLogin={onLogin}/>)
				}
			</UserConsumer>
		</UserProvider>
	)
}

ReactDOM.render(
	
		<CookiesProvider>
			<BrowserRouter>
				<ApolloProvider client={client}>
					<Root />
				</ApolloProvider>
			</BrowserRouter>
		</CookiesProvider>

		, document.getElementById('root'));
serviceWorker.unregister();
