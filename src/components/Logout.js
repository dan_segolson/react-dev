import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import {Mutation} from 'react-apollo'
import { withCookies } from 'react-cookie';
import gql from 'graphql-tag'
import {UserConsumer} from '../utils/UserContext';

const LOGOUT_MUTATION = gql`
  mutation LogoutMutation($app_token: String!, $player_id: String!, $user_token: String!) {
    stage_logout(app_token: $app_token, player_id: $player_id, user_token: $user_token) {
        iLoginStatus
        iPlayerId
        error
    }
  }
`

class Logout extends Component {
    state = {
        app_token: API_TOKEN,
        error_message: '',
        alert_class: '', 
    }
    render(){
        const {app_token} = this.state
        return (
            <UserConsumer>
            {({authenticated, player_id, user_token, onLogin, onLogout}) => (
                <div className="text-center">
                {authenticated && (
                    <Mutation 
                        mutation = {LOGOUT_MUTATION}
                        variables = {{app_token, player_id, user_token}} 
                        onCompleted = {data => this._confirm(data,onLogout)}
                    >
                    {mutation => (
                        <button 
                            type="submit" 
                            className="btn btn-danger"
                            onClick={mutation}>
                            Logout
                        </button>  
                    )}
                    </Mutation>
                )}                                        
                </div>
                )}
            </UserConsumer>
        )
    }

    _confirm = async (data,onLogout) => {
        const logout = data.stage_logout
        this.onLogout = onLogout
        if(logout){
            if(logout.iLoginStatus === 'Logged out'){
                this.onLogout()
                this.props.history.push('/login')
            }else{
                this.setState({alert_class: 'text-danger',error_message: 'Failed logout, no such logged in user'})
                this.onLogout()
            }
        }else{
            console.log('No response')
            this.setState({alert_class: 'text-danger',error_message: 'Failed logout'})
        }
    }
}

export default withCookies(Logout)

