import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import { withCookies } from 'react-cookie'
import Auth from './Auth';
import {UserConsumer} from '../utils/UserContext';

const RATINGS_QUERY = gql`
  query playerRatingsQuery($app_token: String!, $player_id: String!, $user_token: String!, $char_id: String, $lb_type: String!, $limit: String!, $year: String!, $month: String!) {
    stage_player_ratings(app_token: $app_token, player_id: $player_id, user_token: $user_token, char_id: $char_id, lb_type: $lb_type, limit: $limit, year: $year, month: $month) {
        status
	    getPlayerRatings {
	      Points
	      Username
	      PlayerId
	    }
    }
  }
`



class Ratings extends Component {


	state = {
        app_token: API_TOKEN,
        user_token: '', 
        player_id: '', 
        char_id: '1', 
        lb_type: 'getPlayerRatings', 
        limit: '50', 
        year: '2018', 
        month: '12',
        error_message: '',
        alert_class: '', 
    }
    render(){
        const {app_token, char_id, lb_type, limit, year, month} = this.state
        var ratingsList
		return(
			<UserConsumer>
  			{({authenticated, player_id, user_token}) => (
			<div>
			<Auth />
				{console.log(authenticated)}
				{authenticated && (

				<Query 
					query={RATINGS_QUERY}
					variables={{app_token, player_id, user_token, char_id, lb_type, limit, year, month}}
					>
				    {({ loading, error, data }) => {
			          if (loading) return <div>Fetching</div>
			          if (error) return <div>Error {error}</div>
		          		const ratings = data.stage_player_ratings.getPlayerRatings
			      		ratingsList = ratings.map((rating) => <li key={rating.PlayerId}>{rating.Username} - Points: {rating.Points}</li>);			        
			          return (
			            <div>
				            {ratingsList}
			            </div>
			          )
			        }}
			  </Query>
			  )}
			</div>
			)}
			</UserConsumer>
		)
	}

	

}

export default withCookies(Ratings)
