import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import { withCookies } from 'react-cookie'
import Auth from './Auth';
import {UserConsumer} from '../utils/UserContext';

const CHAR_QUERY = gql`
query allCharactersQuery($app_token: String!, $player_id: String!, $user_token: String!) {
    stage_all_characters(app_token: $app_token, player_id: $player_id, user_token: $user_token) {
      CharacterId
      CharName
    }
  }
`



class Chars extends Component {


	state = {
        app_token: API_TOKEN,
        user_token: '', 
        player_id: '', 
        char_id: '1', 
        error_message: '',
        alert_class: '', 
    }
    render(){
        const {app_token} = this.state
        var charList
		return(
			<UserConsumer>
  			{({authenticated, player_id, user_token}) => (
			<div>
			<Auth />
				{console.log(authenticated)}
				{authenticated && (

				<Query 
					query={CHAR_QUERY}
					variables={{app_token, player_id, user_token}}
					>
				    {({ loading, error, data }) => {
				    	console.log(data.stage_all_characters)
			          if (loading) return <div>Fetching</div>
			          if (error) return <div>Error {error}</div>
		          		const chars = data.stage_all_characters
			      		charList = chars.map((char) => <li key={char.CharacterId}>{char.CharName}</li>);			        
			          return (
			            <div>
				            {charList}
			            </div>
			          )
			        }}
			  </Query>
			  )}
			</div>
			)}
			</UserConsumer>
		)
	}

	

}

export default withCookies(Chars)
