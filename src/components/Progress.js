import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import { withCookies } from 'react-cookie'
import Auth from './Auth'
import {UserConsumer} from '../utils/UserContext';

const PROGRESS_QUERY = gql`
   query getPlayerProgress($app_token: String!, $player_id: String!, $user_token: String!) {
	  stage_player_progress(app_token: $app_token, player_id: $player_id, user_token: $user_token){
	      earned_xp
	    	points_for_next_level
	    	cleared_levels{
	    		ProgressId
		        Level
		        XpRequirement
		        RewardId
	      }
	    	all_levels{
	    		ProgressId
		        Level
		        XpRequirement
		        RewardId
	      }
	  }  
}
`


class Progress extends Component{

	state = {
        app_token: API_TOKEN,
        user_token: '', 
        player_id: '', 
        error_message: '',
        alert_class: '', 
    }
console
	render() {
		const {app_token} = this.state
        var progressList

		return (
			<UserConsumer>
  			{({authenticated, player_id, user_token}) => (
			<div>
			<Auth />
			{console.log(authenticated)}
				{authenticated && (
				<Query query={PROGRESS_QUERY}
				variables={{app_token, player_id, user_token}}
				>
				    {({ loading, error, data }) => {
				    	//console.log(data.stage_player_progress)
			          if (loading) return <div>Fetching</div>
			          if (error) return <div>Error: {error}</div>
			          	const p_cleared_prog = data.stage_player_progress.cleared_levels
			            const p_all_prog = data.stage_player_progress.all_levels
			          	const p_prog_base = data.stage_player_progress
			          	
			      		const progressionList = p_cleared_prog.map((prog) => <li key={prog.ProgressId}>{prog.Level}, Req: {prog.XpRequirement}</li>);
			      		const allProgressions = p_all_prog.map((all_prog) => <li key={all_prog.ProgressId}>{all_prog.Level}, Req: {all_prog.XpRequirement}</li>);
			          return (
			          	
			            <div>
			            	<p>Cleared player progression for player-id: {player_id} with Earned XP: {p_prog_base.earned_xp}</p>
				            {progressionList}
				            <p>All levels</p>
				            {allProgressions}
			            </div>
			          )
			        }}
				  </Query>
				)}
			</div>
			)}
		</UserConsumer>
		)
	}
}

export default withCookies(Progress) 

