import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter} from 'react-router-dom'
import { withCookies } from 'react-cookie'
import Login from './Login';
import Logout from './Logout';
import Rewards from './Rewards';
import Progress from './Progress';
import Ratings from './Ratings';
import Chars from './Chars';
import Main from './Main';
import Navigation from './Navigation';
import {UserConsumer} from '../utils/UserContext';



class App extends Component {
  render() {
    return (
    	
    	<UserConsumer>
      		{({authenticated, player_id, user_token}) => (
      	<div>
      		{console.log(authenticated)}
      		{authenticated && (
    			<Redirect to='/main' />
    		)}
		    <Navigation />
	      	<Switch>
	            <Route exact path="/" render= {() => <Redirect to='/login' />} />
	            <Route exact path="/main" component={withRouter(Main)} />
	            <Route exact path="/ratings" component={withRouter(Ratings)} />
	            <Route exact path="/chars" component={withRouter(Chars)} />
	            <Route exact path="/rewards" component={withRouter(Rewards)} />
	            <Route exact path="/progress" component={withRouter(Progress)} />
	            <Route exact path="/login" component={withRouter(Login)} />
	            <Route exact path="/logout" component={withRouter(Logout)} />
	        </Switch>
	    </div>
	    )}
      </UserConsumer>
    )
  }
}
export default withCookies(App)
