import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withCookies } from 'react-cookie'
import {UserConsumer} from '../utils/UserContext';




class Navigation extends Component {


	render(){
		return  (
			<UserConsumer>
  				{({authenticated, player_id, user_token}) => (
					<div>
					{authenticated && (
					<ul className="nav nav-pills nav-fill">

				        <li className="nav-item">
					        <Link to="/main" className="nav-link">
					          Home
					        </Link>
				        </li>
				        <li className="nav-item">
					        <Link to="/chars" className="nav-link">
					          Characters
					        </Link>
				        </li>
				        <li className="nav-item">
					        <Link to="/ratings" className="nav-link">
					          Player Ratings
					        </Link>
				        </li>
				        <li className="nav-item">
					        <Link to="/rewards" className="nav-link">
					          Ingame rewards
					        </Link>
					    </li>
				        <li className="nav-item">
					        <Link to="/progress" className="nav-link">
					          Progress
					        </Link>
				        </li>
				        <li className="nav-item">
					        <Link to="/logout" className="nav-link">
					          Logout
					        </Link> 
					    </li>
					</ul>
					)
				}
				</div>
				)
  			}
			</UserConsumer>
		)}
}

export default withCookies(Navigation)