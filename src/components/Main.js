import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import { withCookies } from 'react-cookie'
import Auth from './Auth';
import {UserConsumer} from '../utils/UserContext';
/*import grindModeImg from '../media/grindMode.png';
import contenderModeImg from '../media/contenderMode.jpg';
import trialModeImg from '../media/trialMode.png';*/

const CHAR_QUERY = gql`
query allCharactersQuery($app_token: String!, $player_id: String!, $user_token: String!) {
    stage_all_characters(app_token: $app_token, player_id: $player_id, user_token: $user_token) {
      CharacterId
      CharName
    }
  }
`
const EVENT_QUERY = gql`
query getAllEvents($app_token: String!, $player_id: String!, $user_token: String!) {
  stage_all_events(app_token: $app_token, player_id: $player_id, user_token: $user_token){
      event_data{
        EventId
		EventName
        EventDescription
      }
      event_participants
  }  
}
`




class Main extends Component {


	state = {
        app_token: API_TOKEN,
        user_token: '', 
        player_id: '', 
        char_id: '1', 
        error_message: '',
        alert_class: '', 
    }
    render(){
    	const gameSlider = (
			<div className="container-lg my-3">
		    <div id="myCarousel" className="carousel slide" data-ride="carousel">
		        <ol className="carousel-indicators">
		            <li data-target="#myCarousel" data-slide-to="0"></li>
		            <li data-target="#myCarousel" data-slide-to="1" className="active"></li>
		            <li data-target="#myCarousel" data-slide-to="2"></li>
		        </ol>
		        <div className="carousel-inner">
		            <div className="carousel-item active">
		                <img src="images/grindMode.png" alt="Grind mode" />
		                <div className="carousel-caption d-none d-md-block">
			                <h2>Grind mode</h2>
			                <p>In need of SP for contendercards? Grind here to get what you need</p>
			            </div>
		            </div>
		            <div className="carousel-item">
		                <img src="images/contenderMode.jpg" alt="Contender mode" />
		                <div className="carousel-caption d-none d-md-block">
			                <h2>Contender mode</h2>
			                <p>Competitive play here, use your unlocked chars and compete</p>
			            </div>
		            </div>
		            <div className="carousel-item">
		                <img src="images/trialMode.png" alt="Trial mode" />
		                <div className="carousel-caption d-none d-md-block">
			                <h2>Trial mode</h2>
			                <p>Learn the features, tricks and skills of the game</p>
			            </div>
		            </div>
		        </div>
		        <a className="carousel-control-prev" href="#myCarousel" data-slide="prev">
		            <span className="carousel-control-prev-icon"></span>
		        </a>
		        <a className="carousel-control-next" href="#myCarousel" data-slide="next">
		            <span className="carousel-control-next-icon"></span>
		        </a>
		    </div>
		</div>
		);
        const {app_token} = this.state
        var eventList
		return(
			<UserConsumer>
  			{({authenticated, player_id, user_token}) => (
			<div>
			<Auth />
				{console.log(authenticated)}
				{authenticated && (

				<Query 
					query={EVENT_QUERY}
					variables={{app_token, player_id, user_token}}
					>
				    {({ loading, error, data }) => {
				    	console.log(data.stage_all_events)
			          if (loading) return <div>Fetching</div>
			          if (error) return <div>Error {error}</div>
		          		const events = data.stage_all_events
			      		eventList = events.map((event) => <li key={event.event_data.EventId}>{event.event_data.EventName}</li>);			        
			          return (
			            <div>
				            {gameSlider}
				            {eventList}
			            </div>
			          )
			        }}
			  </Query>

			  )}
			</div>
			)}
			</UserConsumer>
		)
	}

	

}

export default withCookies(Main)
