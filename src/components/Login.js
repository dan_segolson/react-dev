import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import {Mutation} from 'react-apollo'
import { withCookies } from 'react-cookie'
import {UserConsumer} from '../utils/UserContext';

import gql from 'graphql-tag'

export const LOGIN_MUTATION = gql`
  mutation LoginMutation($app_token: String!, $username: String!, $password: String!) {
    stage_login(app_token: $app_token, username: $username, password: $password) {
    		iLoginStatus
	    	iPlayerId
	    	sUserName
	      	sLoginToken	
	      	error
    }
  }
`

class Login extends Component {
	state = {
		app_token: API_TOKEN,
		username: '',
		password: '',
		error_message: '',
		alert_class: '', 
	}
	render(){
		const {app_token, username, password, alert_class} = this.state 

		return (
			<UserConsumer>
  			{({authenticated, player_id, user_token, onLogin, onLogout}) => (  				
			<div>
			{console.log(authenticated)}
				<section className="login-page">
		            <div className="container flex-center">
		                <div className="d-flex align-items-center">
		                    <div className="row flex-center pt-5 mt-3">
		                        <div className="col-md-6 text-center text-md-left mb-5">
		                            <div className="">
		                                <h1 className="display-4 ">Login</h1>
		                                <hr className="hr-light " />
		                                <h6 className="" >Welcome. Naturally you need to log in to access this, please give it a try</h6>
		                            </div>
		                        </div>
		                        <div className="col-md-6 col-xl-5 offset-xl-1">

		                        <div className="card">
                                    <div className="card-body z-depth-2">
										<div className="text-center">
											<h3>Login</h3>
											<div className={alert_class} role="alert">
											{this.state.error_message}
											</div>
											<hr />
										</div>
										<div className="md-form">
											<i className="fa fa-user prefix grey-text"></i>
											<input
												className="form-control"
										        value={username}
										        onChange={e => this.setState({ username: e.target.value })}
										        type="text"
										        placeholder=""
									        />
									        <label htmlFor="username">Username</label>
									    </div>
									    <div className="md-form">
											<i className="fa fa-lock prefix grey-text"></i>
											<input 
												className="form-control"
												value={password} 
												onChange={e => this.setState({password: e.target.value})} 
												type="password" 
												placeholder=""
											/>
											<label htmlFor="password">Password</label>
										</div>
										<div className="text-center">
											<Mutation 
												mutation = {LOGIN_MUTATION}
												variables = {{app_token, username, password}} 
												onCompleted = {data => this._confirm(data)}
												>
											{mutation => (
												<button 
		                                        	type="submit" 
		                                        	className="btn btn-primary"
		                                        	onClick={mutation}>
	                                        		Login
	                                        	</button>  
											)}
											</Mutation>
	                                                                            
	                                    </div>
                                    </div>
                                </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	    	</section>

		</div>
		)}
  		</UserConsumer>
		)
	}

	_confirm = async (data) => {
		const user = data.stage_login
		if(!user){
			this.setState({alert_class: 'text-danger',error_message: 'Failed login'})
		}else{
			this.setState({alert_class: 'text-success', error_message: 'Successful login'})
			let loginData = {player_id: user.iPlayerId, user_token: user.sLoginToken, username: user.sUserName}
			this.props.onLogin(loginData)	
		}
	}


	
}

export default withCookies(Login)


