import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import {Redirect} from 'react-router-dom'
import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import { withCookies } from 'react-cookie';
import {UserConsumer} from '../utils/UserContext';


export const AUTH_QUERY = gql`
   query authPlayer($app_token: String!, $player_id: String!, $user_token: String!) {
    login_status(app_token: $app_token, player_id: $player_id, user_token: $user_token) {
		status
    }
  }
`

class Auth extends Component{

	state = {
		logged_in: '',
        app_token: API_TOKEN,
        
    }

    render(){
    	const {app_token} = this.state
    	return(
    		<UserConsumer>
  			{({authenticated, player_id, user_token, onLogin, onLogout}) => (
    		<div>
    		{user_token && (
    		<Query 
				query={AUTH_QUERY}
				variables={{app_token, player_id, user_token}}
				>
			    {({ loading, error, data }) => {
		          if (error) return <div>Error {error}</div>
		          	if(data){
		          		//console.log(data.login_status)	
		          		if(data.login_status === undefined){
			          		this.login_status = 'authenticated'
			          		//console.log(this.login_status)
			          	}else{
			          		this.login_status = data.login_status.status
			          		//console.log(this.login_status)
			          	}
			          	
			          	if((this.login_status === 'invalid token') || (this.login_status === 'expired')|| (this.login_status === 'Expired')){
			          		console.log('Removing data from storage')
                			this.props.cookies.remove('user_context')
							onLogout()
					        return(<Redirect to='/login' />)
			          	}
		          	}
			        
		          return (
		          	<div>{this.state.logged_in}</div>
		          )
		        }}
			  </Query>
			 )}
    		{!authenticated && (
    			<Redirect to='/login' />
    		)}
    		</div>
    		)}
    		</UserConsumer>
    	)

    }

}

export default withCookies(Auth)
