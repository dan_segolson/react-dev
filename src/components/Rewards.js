import {API_TOKEN} from '../utils/constants'
import React,{Component} from 'react'
import { withCookies } from 'react-cookie'
import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import {UserConsumer} from '../utils/UserContext';
import Auth from './Auth'

export const INGAME_REWARDS_QUERY = gql`
   query ingameRewards($app_token: String!, $player_id: String!, $user_token: String!) {
    stage_game_rewards(app_token: $app_token, player_id: $player_id, user_token: $user_token) {
		RewardId
		RewardType
		RewardName
		RewardDescription
		RewardsIdentifier
		ItemId
		RewardAmount
		CharId    
    }
  }
`


class Rewards extends Component{

	state = {
        app_token: API_TOKEN,
        error_message: '',
        alert_class: '', 
    }

	render() {
		const {app_token} = this.state
		return (
		<UserConsumer>
  			{({authenticated, player_id, user_token}) => (
			<div>
			<Auth />
				{authenticated && (
				<Query 
					query={INGAME_REWARDS_QUERY}
					variables={{app_token, player_id, user_token}}
					>
				    {({ loading, error, data }) => {
			          if (loading) return <div>Fetching</div>
			          if (error) return <div>Error</div>
			          	//console.log(data)
			          	const rewards = data.stage_game_rewards
			      		const rewardsList = rewards.map((reward) => <li key={reward.RewardId}>{reward.RewardName}. {reward.RewardDescription}, type: {reward.RewardType}</li>);
			          return (
			            <div>
				            {rewardsList}
			            </div>
			          )
			        }}
				</Query>
				)}
			  </div>
			)}
		</UserConsumer>
		)
	}
}

export default withCookies(Rewards)

