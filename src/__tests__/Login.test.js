import React from 'react';
import Enzyme, { mount } from 'enzyme';
import ReactDOM from 'react-dom';
import TestRenderer from 'react-test-renderer'; 
import Login from '../components/Login';
import { UserProvider, UserConsumer } from '../utils/UserContext';
import { MockedProvider } from 'react-apollo/test-utils';
import { LOGIN_MUTATION } from '../components/Rewards';


const mocks = [
  {
    request: {
      query: LOGIN_MUTATION,
    },
    result: {
	  "data": {
	    "stage_login": {
	      "iLoginStatus": "True",
	      "sLoginToken": "516ba77ce32cb38d4c6712b4bcda9be5",
	      "iPlayerId": "1",
	      "error": null
		}
	}
  },
}
];


it('renders without error', () => {
  TestRenderer.create(
    <MockedProvider mocks={mocks} >
      <UserProvider value={mockUserContext} >
        <Login />
      </UserProvider>
    </MockedProvider>,
  );
});

