import React from 'react';
import Enzyme, { mount } from 'enzyme';
import ReactDOM from 'react-dom';
import TestRenderer from 'react-test-renderer'; 
import { BrowserRouter } from 'react-router-dom';
import { MockedProvider } from 'react-apollo/test-utils';
import { UserProvider, UserConsumer } from '../utils/UserContext';
import App from '../components/App';

beforeEach(() => {
  jest.resetModules();
});


it('renders without crashing', () => {
  const div = document.createElement('div');
  const wrapper = mount(
  	<BrowserRouter>
	  	<MockedProvider> 
	  		<UserProvider value={mockUserContext} >
				<App />
			</UserProvider>
		</MockedProvider>
	</BrowserRouter>
  	, div);
  ReactDOM.unmountComponentAtNode(div);
});
