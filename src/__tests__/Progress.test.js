import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import TestRenderer from 'react-test-renderer'; 
import Progress from '../components/Progress';
import { MockedProvider } from 'react-apollo/test-utils';
import { UserProvider, UserConsumer } from '../utils/UserContext';
import { PRIZES_QUERY } from '../components/Rewards';

const mocks = [
  {
    request: {
      query: PRIZES_QUERY,
    },
    result: {
	  "data": {
	    "rewards": {
	      "prizes": [
		        {
		          "prizeId": "bb2bc31b-b0d1-4b6f-a5df-a2e06a3c7fda",
		          "title": "Awarded SP",
		          "description": "A package of 100 SP for your use",
		          "prizeInfoType": "VENDOR"
		        },
		        {
		          "prizeId": "61975d6d-c58f-4870-83ca-85ee09fb8bd2",
		          "title": "Awarded SP",
		          "description": "A package of 500 SP for your use",
		          "prizeInfoType": "VENDOR"
		        },
		        {
		          "prizeId": "5dd603da-c9a3-41f1-bf47-ac8b5a12ae94",
		          "title": "500 Awarded SP",
		          "description": "A package of 500 SP for your use",
		          "prizeInfoType": "VENDOR"
		        },
		        {
		          "prizeId": "abd271c0-804a-4fc1-bf34-97c3396de541",
		          "title": "Awarded SP",
		          "description": "A package of 500 SP for your use",
		          "prizeInfoType": "VENDOR"
		        },
		        {
		          "prizeId": "63ed20a1-bf4f-4ff8-aaea-9062128fe573",
		          "title": "Awarded SP",
		          "description": "A package of 100 SP for your use",
		          "prizeInfoType": "VENDOR"
		        }
	        	]
	    	}
		}
	}
  },
];


it('renders without error', () => {
  TestRenderer.create(
  	<BrowserRouter>
	    <MockedProvider mocks={mocks} addTypename={false}>
	    	<UserProvider value={mockUserContext} >
	      		<Prizes />
	      	</UserProvider>
	    </MockedProvider>
    </BrowserRouter>,
  );
});