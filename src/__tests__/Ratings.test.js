import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import TestRenderer from 'react-test-renderer'; 
import Ratings from '../components/Ratings';
import { MockedProvider } from 'react-apollo/test-utils';
import { UserProvider, UserConsumer } from '../utils/UserContext';
import { RATINGS_QUERY } from '../components/Rewards';

const mocks = [
  {
    request: {
      query: RATINGS_QUERY,
    },
    result: {
	  "data": {
	    "stage_game_rewards": [
	      {
	        "RewardId": "1",
	        "RewardType": "badge",
	        "RewardName": "Arc initiated",
	        "RewardDescription": "Your contender has initiated it&#039;s progression arc and earned the first 5 XP",
	        "RewardsIdentifier": "ROOKIE_BADGE",
	        "ItemId": "0",
	        "RewardAmount": "5",
	        "CharId": null
	      },
	      {
	        "RewardId": "2",
	        "RewardType": "edge_unlock",
	        "RewardName": "Chip and a Chair",
	        "RewardDescription": "You have just unlocked Chip and a Chair based on Cat's edge",
	        "RewardsIdentifier": "EDGE_ABILITY_UNLOCK",
	        "ItemId": "10",
	        "RewardAmount": "0",
	        "CharId": "2"
	      }
	    ]
	  }
	}
  },
];



it('renders without error', () => {
  TestRenderer.create(
  	<BrowserRouter>
	    <MockedProvider mocks={mocks} addTypename={false}>
	    	<UserProvider value={mockUserContext} >
	    		<Ratings />
	    	</UserProvider>
	    </MockedProvider>
    </BrowserRouter>,
  );
});