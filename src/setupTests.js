import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { Cookies } from 'react-cookie'
import Enzyme, { shallow, render, mount } from "enzyme";

configure({ adapter: new Adapter() });

//make this available in all test files
global.React = React;
global.shallow = shallow;
global.render = render;
global.mount = mount;

global.mockUserContext = {
    username : '', 
    authenticated: false,
    player_id: '',
    user_token: '',
    cookies: new Cookies()
}