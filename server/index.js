import express from 'express'
import serverRenderer from './middleware/renderer'


const PORT = 4000
const path = require('path')

const app = express()

//Setup & config router
const router = express.Router()

//catch root for server rendered
router.use('^/*$', serverRenderer)
app.use('/*', serverRenderer)

//serve static stuff
router.use(express.static(
	path.resolve(__dirname, '..', 'build'),
	{maxAge: '1d'}
))
app.use(router)
app.disable('x-powered-by')

//start the server
app.listen(PORT, (error) => {
	if(error){
		return console.log('Arrgh you broke the server...', error)
	}else{
		console.log('Server listening on '+ PORT + '...')
	}
})