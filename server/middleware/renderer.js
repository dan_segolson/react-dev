import React from 'react'
import ReactDOMServer from 'react-dom/server'
import {StaticRouter} from 'react-router-dom'
//import server_login from './auth'

import Cookies from 'universal-cookie';
//here we have the main app component, change path as needed
import { UserProvider, UserConsumer } from '../../src/utils/UserContext'
import App from '../../src/components/App'

const ServerUserContext = {
    username : '', 
    authenticated: false,
    player_id: '',
    user_token: '',
    cookies: new Cookies()
}


const path =  require("path") 
const fs = require("fs") //file-system stuff

export default (req, res, next) => { 

console.log('server called')
console.log(req.headers.cookie)

	const cake = new Cookies(req.headers.cookie)
	const login_data = cake.get('user_context')
console.log(login_data)

	
	if(!login_data){
		ServerUserContext.authenticated = false
		ServerUserContext.username = null
		ServerUserContext.player_id = null
		ServerUserContext.user_token = null
	}else if(login_data.user_token !== ''){
		ServerUserContext.authenticated = true
		ServerUserContext.username = login_data.username
		ServerUserContext.player_id = login_data.player_id
		ServerUserContext.user_token = login_data.user_token
	}
console.log(ServerUserContext)

	//grab the built html file, adjust as needed
	const filePath = path.resolve(__dirname, '..', '..', 'build', 'index.html')
	fs.readFile(filePath, 'utf8', (err, htmlData) => {
		if(err){
			console.error('err', err)
			return res.status(404).end()
		}
		//no error, render app as string
		const context = {}		
		const html = ReactDOMServer.renderToString(
			<UserProvider value={ServerUserContext} >
				<StaticRouter location={req.url} context={context}>			
					<App />
				</StaticRouter>
			</UserProvider>
			)
			//add app to html and return it
			return res.send(
			htmlData.replace(
			'<div id="root"></div>',
			`<div id="root">${html}</div>`	
			)
		)
	})
}